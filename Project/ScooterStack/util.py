import mysql
import mysql.connector
import sys
from config import db_config
from user import User
import random
def basic_hash(text):
    text_int_list = []
    for item in text:
        text_int_list.append(str(ord(item)))
    text_int_list = ''.join(text_int_list)
    int_text = int(text_int_list)
    mult = 6571124425203826399305484198005679260949
    remainder = 8334203544772658688568069417389950195363
    #hashed_text = int_text * len(str(int_text))
    hashed_text = int_text * mult % remainder #Thanks to Peter from the Leeds Discord server for this better hash.
    return hashed_text
#This file should contain the functions that communicate with the database. If we need any functions NOT related to the database, please create a new file called "app_functions.py". (Or something along those lines at least)

#This function gets all of the depots and their info from the database
def get_depots():
    depot_list = []
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM depots")
    all_depots = cursor.fetchall()
    for depots in all_depots:
        depot_list.append({'depot_id': depots[0], 'depot_name': depots[1], 'depot_loc': depots[2], 'depot_status': depots[3], "depot_imagename": depots[4]})
    return depot_list

#Attempts to sign the user in
def validate_user(uinfo):
    uinfo = uinfo.split("+")
    current_user = None
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_uname = %s", (str(uinfo[0]),))
    retrieved_user = cursor.fetchall()
    if len(retrieved_user) > 0:
        print("Username Accepted", file=sys.stderr)
        for item in retrieved_user:
            uinfo[1] = basic_hash(uinfo[1])
            if str(uinfo[1]) == str(item[4]):
                print("Password Accepted", file=sys.stderr)
                current_user = User(item[0], item[3])
            else:
                print(uinfo[1] == item[4])
                print("Password Denied", file=sys.stderr)
    else:
        print("Username Denied")
    return current_user

def get_user(id): #Retrieves the user information for a user that has already signed in
    user = []
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_id_no = '" + str(id) + "'")
    cfetch = cursor.fetchall()
    for item in cfetch:
        user.append(item[0])
        user.append(item[3])
        user.append(item[5])
    return user

def get_user_info(username): #Gets the user's information from the database
    user_info = {}
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_uname = %s", (str(username),))
    retrieved_user = cursor.fetchall()
    if len(retrieved_user) > 0:
        for item in retrieved_user:
            user_info["user_id"] = item[0]
            user_info["user_forename"] = item[1]
            user_info["user_surname"] = item[2]
            user_info["user_username"] = item[3]
            user_info["user_email"] = item[5]
            user_info["user_phone"] = item[6]
    return user_info

def create_user_account(userinfo): #Creates the user account
    success_status = False
    userinfo = userinfo.split("+")
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_uname = %s", (str(userinfo[2]),))
    retrieved_user = cursor.fetchall()
    print("Users found: " + str(len(retrieved_user)), file=sys.stderr)
    if len(retrieved_user) <= 0:
        #0, "TEST", "USER", "TESTUSER", "ADMIN", "N/A", "N/A"
        cursor.execute("SELECT user_id_no FROM users")
        new_user_id = len(cursor.fetchall())
        if (userinfo[4] == ""):
            userinfo[4] = "N/A"
        if (userinfo[5] == ""):
            userinfo[5] = "N/A"
        #Password needs to be HASHED!!!! Get on that.
        userinfo[3] = basic_hash(userinfo[3])
        cursor.execute("INSERT INTO users VALUES(%s, %s, %s, %s, %s, %s, %s)", (new_user_id, userinfo[0], userinfo[1], userinfo[2], userinfo[3], userinfo[4], userinfo[5]))
        database.commit()
        success_status = True
    return success_status

def delete_user_account(uname): #Deletes the user's account after deleting their id from any linked tables.
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT user_id_no FROM users WHERE user_uname = %s", (str(uname),))
    for item in cursor.fetchall():
        user_id = item
    cursor.execute("DELETE FROM user_depots WHERE user_id_no = %s", (str(user_id[0]),))
    database.commit()
    cursor.execute("DELETE FROM user_scooters WHERE user_id_no = %s", (str(user_id[0]),))
    database.commit()
    cursor.execute("DELETE FROM users WHERE user_uname = %s", (str(uname),))
    database.commit()
    success_status = True

def depot_search(address):
    if (address != None):
        print(len(address), file=sys.stderr)
        if len(address) >= 4:
            address = address[2] + address[3]
        else:
            address = address + " "
            address = address[2] + address[3]
        address = int(address)
        depot_info = {}
        if address <= 2 and address <= 10:
            depot_id = 0
        elif address > 10 and address <= 23:
            depot_id = 2
        else:
            depot_id = 1
        database = mysql.connector.connect(**db_config)
        cursor = database.cursor()
        cursor.execute("SELECT * FROM depots WHERE depot_id = %s", (str(depot_id),))
        for item in cursor.fetchall():
            depot_info["depot_id"] = item[0]
            depot_info["depot_name"] = item[1]
            depot_info["depot_location"] = item[2]
            depot_info["depot_open"] = item[3]
            depot_info["depot_imagename"] = item[4]
        return depot_info
    else:
        return None