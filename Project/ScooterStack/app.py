#(This file controls the website)
from flask import Flask, render_template, url_for, request, redirect
from flask_login import LoginManager, UserMixin, current_user, login_user, logout_user
import sys
from util import get_depots, validate_user, get_user, get_user_info, create_user_account, delete_user_account, depot_search
from user import User

#User accounts
login_manager = LoginManager()
app = Flask(__name__)
app.secret_key = "OHBOY"
login_manager.init_app(app)
@login_manager.user_loader
def load_user(id):
    user_check = get_user(id)
    if len(user_check) <= 0:
        return User(-999, "-999USERDOESNOTEXIST")
    else:
        return User(user_check[0], user_check[1])
def get_username_auth():
    if current_user.is_authenticated:
        user=current_user.username
    else:
        user=None
    return user

#Main Routes
@app.route('/') #Home Page
def home():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    return render_template('home.html', page_name="Home", user=get_username_auth())
@app.route('/about') #About Page
def about():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    return render_template('about.html', page_name="About", user=get_username_auth())
@app.route('/depots') #Depots Page
def depots():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    return render_template('depots.html', page_name="Depots", depot_info=get_depots(), user=get_username_auth())
@app.route('/booking/failed=<booking_failed>')
@app.route('/booking/failed=<booking_failed>/search=<search>')
@app.route('/booking/falied=<booking_falied>')
def booking(booking_failed, search=None):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
        else:
            if search != None:
                search = search.upper()
                if search[0] != "L" or search[1] != "S":
                    return redirect('/booking/failed=True')
                if str(search[2]).isdigit is False or str(search[3]).isdigit() is False:
                    return redirect('/booking/failed=True')
            return render_template('booking.html', page_name="Booking", user=get_username_auth(), depots=depot_search(search))
    else:
        return redirect('/login/failed=False')

#Account Routes
@app.route('/login/failed=<login_failed>') #Login Page
def login(login_failed):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    print(login_failed, file=sys.stderr)
    if current_user.is_authenticated:
        return redirect('/')
    else:
        return render_template('login.html', page_name="Login", login_status=login_failed)
@app.route('/login/userinfo=<user_info>') #Login Check Page. Note: User is immediately redirected and no page should be returned.
def login_auth(user_info):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    if current_user.is_authenticated:
        return redirect('/')
    else:
        user = validate_user(user_info)
        try:
            login_user(user)
            return redirect('/')
        except:
            return redirect('/login/failed=True')
@app.route('/user/<username>') #User's account page
def user_account_page(username):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    if current_user.is_authenticated:
        if current_user.username == username:
            return render_template('account.html', page_name=username, user=get_username_auth(), userinfo=get_user_info(username))
        else:
            return redirect('/login/failed=False')
    else:
        return redirect('/login/failed=False')
@app.route('/logout')
def logout():
    print("Remote IP: " + str(request.remote_addr))
    logout_user()
    return redirect('/')
@app.route('/signup/failed=<signup_failed>') #Signup page
def signup(signup_failed):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    if current_user.is_authenticated:
        return redirect('/')
    else:
        return render_template('signup.html', page_name="Signup", user=get_username_auth())
@app.route('/signup/create/userinfo=<user_info>')
def create_account(user_info): #Account Creation
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    signup_status = create_user_account(user_info)
    if signup_status is True:
        return redirect('/login/failed=False')
    else:
        return redirect('/signup/failed=True')
@app.route('/user/delete')
def delete_account():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        delete_user_account(current_user.username)
        return redirect('/logout')
    else:
        return redirect('/login')
if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)