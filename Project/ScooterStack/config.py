#This file stores the information to connect to the database.
db_config = {
    'user': 'root',
    'password': 'scooters',
    'host': 'localhost',
    'port': '1234',
    'database': 'scooterstack'
}