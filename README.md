The place to save all of our work.
Please DO NOT create separate branches as I will remove them!

Also, before working with the GIT, make sure it is up to date by pulling the most recent version.
If you use GIT using the CMD or PowerShell, use the following command: "git pull" while in the GIT folder.
If you use Visual Studio Code, navigate to source control, click the three dots next to the refresh button then hover over "Pull, Push" and then click "Sync".

And most importantly, please message the whole group if you can't make a meeting or can't do work. But also be sure to give a reason.